const chai = require('chai');
const { expect } = require('chai');
// can also be const expect = chai.expect;

const http = require('chai-http');
chai.use(http);

describe('api_test_suite', ()=>{
	
	//this is an assertation using chai's "expect"
	// it will check if the res object is not equal to "undefined"
	it('test_api_get_people_is_running', ()=>{
		chai.request('http://localhost:5001').get('/people').end((err, res)=>{
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_get_people_return_200', (done)=>{
		chai.request('http://localhost:5001').get('/people').end((err, res)=>{
			expect(res.status).to.equal(200)
			//this is an assertation that checks if the repsonse is ok
			done()
			//a way or function in Mocha that makes sure that this async function is completed. 
			//used to indicate that the async test is complete.
		})
	})

	//for post
	it('test_api_post_person_returns_400_if_no_person_name', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "James",
			age: 28
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done()
		})
	})


	//activity
	it('test_api_post_person_is_running', () => {
		chai.request('http://localhost:5001')
		.post('/person')
		.end((err, res)=>{
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_post_person_returns_400_if_no_ALIAS', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "adminJames",
			age: 28
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_api_post_person_returns_400_if_no_AGE', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "James",
			alias: "adminJames"
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done()
		})
	})

})
//activity for users
describe('api_test_suite_users', () => {
	it('test_api_post_user_is_running', () => {
		chai.request('http://localhost:5001')
		.post('/users')
		.end((err, res)=>{
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_post_user_returns_400_if_no_USERNAME', (done) => {
		chai.request('http://localhost:5001')
		.post('/users')
		.type('json')
		.send({
			name: "adminJames",
			age: 28
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_api_post_user_returns_400_if_no_AGE', (done) => {
		chai.request('http://localhost:5001')
		.post('/users')
		.type('json')
		.send({
			name: "James",
			alias: "adminJames"
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done()
		})
	})
})
