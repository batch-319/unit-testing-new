const names = {
	"Brandon":{
		"name":"Brandon Boyd",
		"age":35,
		"alias":"BoydMayWeather"
	},
	"Steve":{
		"name":"Steve Tyler",
		"age": 56,
		"alias":"Tyler the Great"
	}
}

const usernames = {
	"Todd":{
		"username":"user123",
		"age": 24
	},
	"Troy":{
		"username":"admin123",
		"age": 25
	}
}


module.exports = {
	names: names,
	usernames: usernames
}
