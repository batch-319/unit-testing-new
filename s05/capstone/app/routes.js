const { exchangeRates } = require('../src/util.js');



module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

// test capstone s05

	app.post('/currency', (req, res) => {

		let foundExRates = Object.keys(exchangeRates).includes(req.body.alias)

		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error':'Bad Request - NAME has to be a string'
			})
		}

		else if(!req.body.hasOwnProperty('name')){
				return res.status(400).send({
				'error':'Bad Request - missing required parameter currency NAME'
			})
		}

		else if(req.body.name == "" || req.body.name == undefined){
			return res.status(400).send({
				'error':'Bad Request - NAME parameter is empty.'
			})
		}

		

		else if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error':'Bad Request - missing required parameter EX'
			})
		}

		else if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				'error':'Bad Request - EX has to be an object.'
			})
		}

		else if(Object.keys(req.body.ex).length === 0){
			return res.status(400).send({
				'error': 'Bad Request - object ex is missing data/content'
			})
		}

		else if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error':'Bad Request : no currency ALIAS'
			})
		}

		else if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error':'Bad Request : ALIAS is not a string'
			})
		}

		else if(req.body.alias === undefined || req.body.alias === null || req.body.alias === ""){
				return res.status(400).send({
				"error":"Bad Request: alias is empty"
				})
			}

		else if(foundExRates === true && !req.body.hasOwnProperty('alias') && !req.body.hasOwnProperty('name') && !req.body.hasOwnProperty('ex')){
			console.log(foundExRates)
			return res.status(400).send({
				'error':'Bad Request : a duplicate currency alias once found.'
			})
		}

		else if(!foundExRates && req.body.hasOwnProperty('alias') && req.body.hasOwnProperty('name') && req.body.hasOwnProperty('ex')){
			console.log(foundExRates)
			return res.status(200).send('ok')
		}

	})
}
