const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})

	//test api post currency is 200
	it('test_api_post_currency_is_200', (done) => {

		let currency = {
			name: 'United States Dollar',
	      	ex: {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
	        }
		}

		chai.request('http://localhost:5001')
		.post('/currency')
		.send(currency)
		.end((err, res) => {
			expect(res.status).to.not.equal(404);
			done();	
		})		
	})
	
	//test api post currency returns 400 if no currency name

	it('test_api_post_currency_returns_400_if_no_currency_name', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias:"Don",
			ex:{
				'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
				}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	//test api post currency returns 400 if currency name is not a string
	it('test_api_post_currency_returns_400_if_currency_name_is_not_a_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name:24
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	//test api post currency returns 400 if currenct name is an empty string
	it('test_api_post_currency_returns_400_if_currency_name_is_an_empty_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name:""
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	//test api post currency returns 400 if no currency ex
	it('test_api_post_currency_returns_400_if_no_currency_ex', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			unValues:{
        	'peso': 50.73,
        	'won': 1187.24,
        	'yen': 108.63,
        	'yuan': 7.03
    		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	//test api post currency returns 400 if currency ex not an object
	it('test_api_post_currency_returns_400_if_currency_ex_not_an_object', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			ex:24
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	//test api post currency returns 400 if no ex content
	it('test_api_post_currency_returns_400_if_no_ex_content', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			ex:{}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	//test api post currency returns 400 if no currency alias
	it('test_api_post_currency_returns_400_if_no_currency_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'name':"United States Dollar",
			'ex': {
		    	'peso': 50.73,
		    	'won': 1187.24,
		    	'yen': 108.63,
		    	'yuan': 7.03
    		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})	
	})
	//test api post currency returns 400 if alias not a string
	it('test_api_post_currency_returns_400_if_alias_not_a_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: 24,
			name:"United States Dollar",
			ex: {
		    	'peso': 50.73,
		    	'won': 1187.24,
		    	'yen': 108.63,
		    	'yuan': 7.03
    		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})	
	})
	//test api post currency returns 400 if currency alias is an empty string
	it('test_api_post_currency_returns_400_if_currency_alias_is_an_empty_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: ""
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})	
	})

	//test api post currency returns 400 if duplicate alias found
	it('test_api_post_currency_returns_400_if_duplicate_alias_found', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: 'usd',
			name: 'United States Dollar',
	      	ex: {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
	    	}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})
	})

	//test api post currency returns 200 if complete input given
	it('test_api_post_currency_returns_200_if_complete_input_given', (done)=>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyad',
	      	ex: {
	        	'peso': 0.47,
	        	'usd': 0.0092,
	        	'won': 10.93,
	        	'yuan': 0.065
	    	}
		})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})
	})
})
